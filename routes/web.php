<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/* Возврат товара       начало                                   */
//Route::middleware(['auth'])->group(function () {
    Route::get('/', 'ReturnItemController@index')->name('return_items');
    Route::get('/add', 'ReturnItemController@add');
    Route::get('/edit', 'ReturnItemController@edit');
    Route::post('/store', 'ReturnItemController@store');
    Route::put('/update', 'ReturnItemController@update');
    Route::get('/delete', 'ReturnItemController@delete');
    Route::delete('/delete', 'ReturnItemController@delete');
    Route::post('/', 'ReturnItemController@index');
//});

    Route::get('/items','ItemController@index')->name('items');
    Route::get('/items/add', 'ItemController@add');
    Route::get('/items/edit', 'ItemController@edit');
    Route::post('/items/store', 'ItemController@store');
    Route::put('/items/update', 'ItemController@update');
    Route::delete('/items/delete', 'ItemController@delete');
    Route::get('/items/delete', 'ItemController@delete');

    Route::get('/agents','AgentsController@index')->name('agents');
    Route::get('/agents/add', 'AgentsController@add');
    Route::get('/agents/edit', 'AgentsController@edit');
    Route::post('/agents/store', 'AgentsController@store');
    Route::put('/agents/update', 'AgentsController@update');
    Route::delete('/agents/delete', 'AgentsController@delete');
    Route::get('/agents/delete', 'AgentsController@delete');

    Route::get('/reasons','ReasonController@index')->name('reasons');
    Route::get('/reasons/add', 'ReasonController@add');
    Route::get('/reasons/edit', 'ReasonController@edit');
    Route::post('/reasons/store', 'ReasonController@store');
    Route::put('/reasons/update', 'ReasonController@update');
    Route::delete('/reasons/delete', 'ReasonController@delete');
    Route::get('/reasons/delete', 'ReasonController@delete');

    Route::get('/citys','CityController@index')->name('citys');
    Route::get('/citys/add', 'CityController@add');
    Route::get('/citys/edit', 'CityController@edit');
    Route::post('/citys/store', 'CityController@store');
    Route::put('/citys/update', 'CityController@update');
    Route::delete('/citys/delete', 'CityController@delete');
    Route::get('/citys/delete', 'CityController@delete');

    Route::get('/decisions','DecisionController@index')->name('decisions');
    Route::get('/decisions/add', 'DecisionController@add');
    Route::get('/decisions/edit', 'DecisionController@edit');
    Route::post('/decisions/store', 'DecisionController@store');
    Route::put('/decisions/update', 'DecisionController@update');
    Route::delete('/decisions/delete', 'DecisionController@delete');
    Route::get('/decisions/delete', 'DecisionController@delete');

    Route::get('/marg','Raw_margarinController@index')->name('marg_items');;
    Route::get('/marg/add','Raw_margarinController@add');
    Route::get('/marg/edit','Raw_margarinController@edit');
    Route::post('/marg/store','Raw_margarinController@store');
    Route::get('/marg/update','Raw_margarinController@update');
    Route::get('/marg/del','Raw_margarinController@delete');
    Route::delete('/marg/del','Raw_margarinController@delete');


/* Возврат товара  конец                                        */

/* Маргарин       начало                                   */

/* Маргарин       конец                                    */

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
