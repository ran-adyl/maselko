$("#logout_button").click(function () {
    $('form#logout_form').submit();
});

$(document).ready(function () {
    $('body').on('click', 'ul.pagination li a', function (e) {
        e.preventDefault();

        var url = $(this).attr('href');

        getSites(url);
        window.history.pushState("", "", url);
    });

    function getSites(url) {
        var csrftoken = $('meta[name=csrf-token]').attr('content');
        $.ajaxSetup({
            beforeSend: function (xhr, settings) {
                if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type)) {
                    xhr.setRequestHeader("X-CSRF-TOKEN", csrftoken)
                }
            }
        });

        $.ajax({
            method: "POST",
            type: "POST",
            url: url
        }).done(function (data) {
            $('.table-rows').html(data);
        }).fail(function () {
            alert('Не удалось загрузить данные, попробуйте позже.');
        });
    }

    $(".filter").keyup(throttle(function () {
        var filters = {};
        $(".filter").each(function () {
            filters[$(this).attr("id")] = $(this).val();
        });

        filterSites(filters);
    }));

    $("#date_filters").click(function () {
        var filters = {};
        $(".filter").each(function () {
            filters[$(this).attr("id")] = $(this).val();
        });

        filterSites(filters);
    });

    $("#clear_date_filters").click(function () {
        $("#from_date").val("");
        $("#to_date").val("");
        console.log('test');
        var filters = {};
        $(".filter").each(function () {
            filters[$(this).attr("id")] = $(this).val();
        });

        filterSites(filters);
    });

    function filterSites(filters) {
        var csrftoken = $('meta[name=csrf-token]').attr('content');

        $.ajaxSetup({
            beforeSend: function (xhr, settings) {
                if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type)) {
                    xhr.setRequestHeader("X-CSRF-TOKEN", csrftoken)
                }
            }
        });

        $.ajax({
            method: "POST",
            type: "POST",
            data: {
                filters_array: JSON.stringify(filters),
            }
        }).done(function (data) {
            $('.table-rows').html(data);
        }).fail(function () {
            alert('Не удалось загрузить данные, попробуйте позже.');
        });
    }

    /**
     * Delay after last key up event
     * @param f
     * @param delay
     * @returns {Function}
     */
    function throttle(f, delay) {
        var timer = null;
        return function () {
            var context = this, args = arguments;
            clearTimeout(timer);
            timer = window.setTimeout(function () {
                    f.apply(context, args);
                },
                delay || 150);
        };
    }
})

