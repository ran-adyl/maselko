<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReturnItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('return_items', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->dateTime('created_date')->nullable();
            $table->dateTime('returned_date')->nullable();
            $table->dateTime('product_date')->nullable();
            $table->integer('item_id')->nullable();
            $table->integer('quantity')->nullable();
            $table->integer('agent_id')->nullable();
            $table->string('firm', 255)->nullable();
            $table->integer('reason_id')->nullable();
            $table->integer('city_id')->nullable();
            $table->integer('decision_id')->nullable();
            $table->string('reason_analization', 255)->nullable();
            $table->string('correction_actions', 255)->nullable();
            $table->dateTime('decision_edited')->nullable();
            $table->integer('user_id')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('return_items');
    }
}
