<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveColumnCreatedDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('return_items', function (Blueprint $table) {
            $table->dropColumn('created_date');
        });
    }

    public function down()
    {
        Schema::table('return_items', function (Blueprint $table) {
            $table->datetime('created_date');
        });
    }
}
