<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRawMargarines extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('raw_margarines', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('product_id')->nullable(); // id продукта из справочника
            $table->string('batch', 255)->nullable(); // партия
            $table->string('quantity', 255)->nullable(); // количество и нумерация закладок
            $table->integer('kkt1_pasteurization')->nullable(); // ккт1 пастеризация
            $table->integer('kkt2_filtration')->nullable();// ккт2 фильтрация
            $table->integer('net_weight')->nullable();//вес нетто
            $table->integer('packaging')->nullable();// внешний вид упаковки
            $table->integer('consistency')->nullable();
            $table->integer('color')->nullable();//цвет
            $table->integer('taste_smell')->nullable();//вкус и запах
            $table->string('fat', 255)->nullable();//массовая доля жира
            $table->string('humidity', 255)->nullable();//влажность и кислотность
            $table->string('salt', 255)->nullable();//доля соли
            $table->string('preservative', 255)->nullable();//Массовая доля консервантов
            $table->string('avidity_k', 255)->nullable();//Кислотность К
            $table->string('melting_temperature', 255)->nullable();//Температура плавления
            $table->string('coliform_bacteria', 255)->nullable();//БГКП (доля колиформных бактерий)
            $table->string('mold', 255)->nullable();//Плесеньробные
            $table->string('anaerobic_microorganisms', 255)->nullable();//КМАФНМ (анаэ бактерии)А
            $table->string('monitoring', 255)->nullable();//мониторинг
            $table->dateTime('actual_decision_date')->nullable(); // фактическая дата принятия решения
            $table->integer('decisionraw2s_id')->nullable();// id решения
            $table->dateTime('planned_date')->nullable(); // планируемая дата принятия решения
            $table->integer('user_id')->nullable(); // пользователь, который редактировал решение
            $table->string('comments', 255)->nullable();//примечания

        }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Raw1s');
    }
}
