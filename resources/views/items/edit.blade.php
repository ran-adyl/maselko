@extends('layouts.main')
@section('content')
<div class="container">
    <!-- Example row of columns -->
    <div class="row">
        <div class="col-md-12">

            <div class="actions columns large-2 medium-3">
                <ul class="list-inline">
                    <li>
                        <form name="post_5b3373d009b46402802823" style="display:none;" method="post"
                              action="http://lab.maselko.uz/return_items/delete/48607">
                            <input type="hidden" name="_method" value="POST"></form>
                        <a href="{{route('items')}}/delete?id={{$requestItem->id}}" class="btn btn-xs btn-danger"
                           onclick="if (confirm(&quot;Are you sure you want to delete # {{$requestItem->id}}?&quot;))
                                   { document.post_5b2dbb7bb7ab4047066095.submit(); }
                                   event.returnValue = false;
                                   return false;
                                   ">Удалить</a>
                    <li><a href="{{route('items')}}" class="btn btn-xs btn-success"><i class="icon-level-down"></i>
                            Список товаров</a></li>
                </ul>
            </div>

            <div class="returnItems form large-10 medium-9 columns">
                <form method="post" accept-charset="utf-8" role="form" action="{{route('items')}}/update">
                    <div style="display:none;"><input type="hidden" name="_method" value="PUT"></div>
                    <fieldset>
                        <legend>Изменить наименование товара</legend>

                        <div class="form-group"><label class="control-label" for="name">Наименование товара</label><input type="text" name="name" maxlength="255"
                                                       id="name"
                                                       value="{{$requestItem->name}}"
                                                               class="form-control"></div>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="id" value="{{$requestItem->id}}">

                    </fieldset>
                    <button type="submit" class="btn">Сохранить</button>
                </form>
            </div>
        </div>
    </div>

    <hr>

    <footer>
        <p>© Maselko 2016</p>
    </footer>
</div> <!-- /container -->
@endsection