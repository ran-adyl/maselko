
    <!-- ======== ВЫВОД ЗНАЧЕНИЙ ИЗ БАЗЫ ДАННЫХ ========== -->
    @foreach($margItems as $margItem)
        <tr>

<!--            `id`, `created_at`, `updated_at`, `product_id`, `batch`, `quantity`, `kkt1_pasteurization`,
            `kkt2_filtration`, `net_weight`, `packaging`, `consistency`, `color`, `taste_smell`, `fat`,
            `humidity`, `salt`, `preservative`, `avidity_k`, `melting_temperature`, `coliform_bacteria`,
            `mold`, `anaerobic_microorganisms`, `monitoring`, `actual_decision_date`, `planned_date`,
            `user_id`, `comments` -->
            <td><?=$margItem->created_at?></td>
            <td><?=$margItem->product_date?></td>
            <td><?=$margItem->product_id?></td>

            <td><?=$margItem->batch?></td>
            <td><?=$margItem->quantity?></td>
            <td><?=$margItem->kkt1_pasteurization?></td>
            <td><?=$margItem->kkt2_filtration?></td>
            <td><?=$margItem->net_weigth?></td>
            <td><?=$margItem->packaging?></td>
            <td><?=$margItem->consistency?></td>
            <td><?=$margItem->color?></td>
            <td><?=$margItem->taste_smell?></td>
            <td><?=$margItem->fat?></td>
            <td><?=$margItem->himidity?></td>
            <td><?=$margItem->salt?></td>
            <td><?=$margItem->preservative?></td>
            <td><?=$margItem->avidity_k?></td>
            <td><?=$margItem->melting_tempetature?></td>
            <td><?=$margItem->coliform_bacteria?></td>
            <td><?=$margItem->mold?></td>
            <td><?=$margItem->anaerobic_microorganisms?></td>
            <td><?=$margItem->monitoring?></td>
            <td><?=$margItem->actual_decision_date?></td>
            <td><?=$margItem->decisionraw2_id?></td>
            <td><?=$margItem->planned_date?></td>
            <td><?=$margItem->user_id?></td>
            <td><?=$margItem->comments?></td>

<!--            <td>    if($margItem->cities<>NULL)
                    ?=$margItem->cities->name?>
                else ?=NULL?>
                endif
            </td>-->


            <td class="actions">
                <a href="{{route('marg_items')}}/edit?id={{$margItem->id}}">Изменить</a>
                <form name="post_5b2dbb7bb7ab4047066095" style="display:none;" method="post"
                      action="{{route('marg_items')}}/del?id={{$margItem->id}}">
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                </form>
                <a href="{{route('marg_items')}}/del?id={{$margItem->id}}"
                   onclick="if (confirm(&quot;Are you sure you want to delete # {{$margItem->id}}?&quot;))
                                { document.post_5b2dbb7bb7ab4047066095.submit(); }
                                event.returnValue = false;
                                return false;
                      ">Удалить</a>
            </td>
        </tr>

    @endforeach
    <tr>
        <td colspan="15"> {{$margItems->links()}} </td>
    </tr>



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript">

    $(function() {
        $('body').on('click', '.pagination a', function(e) {
            e.preventDefault(); // предотвратить поведение по умолчанию (чтобы не переходил по ссылке)

            var url = $(this).attr('href'); // получает аттрибут href элемета ссылки a, на который кликнули
            getArticles(url);
            window.history.pushState("", "", url); // добавить параметры в браузерную строку, для информативности
        });

        /**
         * Ajax запрос, метод пост, к роуту возвратов товаров
         * @param url
         */
        function getArticles(url) {
            var csrftoken = $('meta[name=csrf-token]').attr('content'); // в <head> нужно добавить мета тег csrf-token
            $.ajaxSetup({
                beforeSend: function (xhr, settings) {
                    if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type)) {
                        xhr.setRequestHeader("X-CSRF-TOKEN", csrftoken)
                    }
                }
            });

            $.ajax({
                method: "POST",
                type: "POST",
                url: url
            }).done(function (data) {
                $('.table-rows').html(data); // заменяет данные в таблице на ответ с контроллера
            }).fail(function () {
                alert('Не удалось загрузить данные, попробуйте позже.');
            });
        }
    });

</script>