
    <!-- ======== ВЫВОД ЗНАЧЕНИЙ ИЗ БАЗЫ ДАННЫХ ========== -->
    @foreach($returnItems as $returnItem)
        <tr>
            <td><?=$returnItem->created_at?></td>
            <td><?=$returnItem->returned_date?></td>
            <td><?=$returnItem->product_date?></td>

            <td>@if($returnItem->items<>NULL)
                    <?=$returnItem->items->name?>
                @else <?=NULL?>
                @endif
            </td>
            <td>@if($returnItem->cities<>NULL)
                    <?=$returnItem->cities->name?>
                @else <?=NULL?>
                @endif
            </td>
            <td><?=$returnItem->quantity?></td>

            <td>@if(($returnItem->agents<>NULL))
                <?=$returnItem->agents->name?>
                @else <?=NULL?>
                @endif</td>

            <td><?=$returnItem->firm?></td>

            <td>@if($returnItem->reasons<>NULL)
                    <?=$returnItem->reasons->name?>
                @else <?=NULL?>
                @endif
            </td>

            <td><?=$returnItem->decision_edited?>
            </td>

            <td>@if($returnItem->decisions<>NULL)
                    <?=$returnItem->decisions->name?>
                @else <?=NULL?>
                @endif
            </td>

            <td><?=$returnItem->user_id?>
            </td>

            <td><?=$returnItem->reason_analization?>
            </td>

            <td><?=$returnItem->correction_actions?>
            </td>

            <td class="actions">
                <a href="{{route('return_items')}}/edit?id={{$returnItem->id}}">Изменить</a>
                <form name="post_5b2dbb7bb7ab4047066095" style="display:none;" method="post"
                      action="{{route('return_items')}}/delete?id={{$returnItem->id}}">
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                </form>
                <a href="{{route('return_items')}}/delete?id={{$returnItem->id}}"
                   onclick="if (confirm(&quot;Are you sure you want to delete # {{$returnItem->id}}?&quot;))
                                { document.post_5b2dbb7bb7ab4047066095.submit(); }
                                event.returnValue = false;
                                return false;
                      ">Удалить</a>
            </td>
        </tr>

    @endforeach
    <tr>
        <td colspan="15"> {{$returnItems->links()}} </td>
    </tr>



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript">

    $(function() {
        $('body').on('click', '.pagination a', function(e) {
            e.preventDefault(); // предотвратить поведение по умолчанию (чтобы не переходил по ссылке)

            var url = $(this).attr('href'); // получает аттрибут href элемета ссылки a, на который кликнули
            getArticles(url);
            window.history.pushState("", "", url); // добавить параметры в браузерную строку, для информативности
        });

        /**
         * Ajax запрос, метод пост, к роуту возвратов товаров
         * @param url
         */
        function getArticles(url) {
            var csrftoken = $('meta[name=csrf-token]').attr('content'); // в <head> нужно добавить мета тег csrf-token
            $.ajaxSetup({
                beforeSend: function (xhr, settings) {
                    if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type)) {
                        xhr.setRequestHeader("X-CSRF-TOKEN", csrftoken)
                    }
                }
            });

            $.ajax({
                method: "POST",
                type: "POST",
                url: url
            }).done(function (data) {
                $('.table-rows').html(data); // заменяет данные в таблице на ответ с контроллера
            }).fail(function () {
                alert('Не удалось загрузить данные, попробуйте позже.');
            });
        }
    });

</script>
