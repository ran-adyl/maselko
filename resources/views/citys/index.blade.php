    @extends('layouts.main')
    @section('content')
    <div class="container">
        <!-- Example row of columns -->
        <div class="row">
            <div class="col-md-12">

                <div class="actions columns large-2 medium-3">

    <ul class="list-inline">
        <li><a href="{{route('citys')}}/add" class="btn btn-xs btn-primary"><i class="icon-plus"></i> Добавить город</a></li>
        <li><a href="http://lab.maselko.uz/.csv" class="btn btn-xs btn-warning"><i class="icon-plus"></i> Экспорт CSV</a></li> 
    </ul>
</div>

<div class="returnItems index large-10 medium-9 columns">
    <table class="table table-striped">
    <thead>
    <!-- НАЗВАНИЯ СТОЛБЦОВ В ТАБЛИЦЕ -->
        <tr>
            <th><a href="http://lab.maselko.uz/?sort=item_id&amp;direction=asc">Город</a></th>
            <th class="actions">Действия</th>
        </tr>
    <!-- - - - - - - - - - - - - - -->
    </thead>

    <tbody>

    <!-- ======== ВЫВОД ЗНАЧЕНИЙ ИЗ БАЗЫ ДАННЫХ ========== -->
    @foreach($citys as $city)
        <tr>

            <td><?=$city->name?>
            </td>

            <td class="actions">
                <a href="{{route('citys')}}/edit?id={{$city->id}}">Изменить</a>
                <form name="post_5b2dbb7bb7ab4047066095" style="display:none;" method="post"
                      action="{{route('citys')}}/delete?id={{$city->id}}">
                    <input type="hidden" name="_method" value="DELETE">
                    {{csrf_field()}}
                </form>
                <a href="{{route('citys')}}/delete?id={{$city->id}}"
                   onclick="if (confirm(&quot;Are you sure you want to delete # {{$city->id}}?&quot;))
                                { document.post_5b2dbb7bb7ab4047066095.submit(); }
                                event.returnValue = false;
                                return false;
                      ">Удалить</a>
            </td>
        </tr>

    @endforeach


        <!-- ================================================ -->
    </tbody>
    </table>
    {{$citys->links()}}
</div>            </div>
        </div>

      <hr>

      <footer>
        <p>© Maselko 2016</p>
      </footer>
    </div> <!-- /container -->
    @endsection
