<!DOCTYPE html>
<!-- saved from url=(0039)http://lab.maselko.uz/ReturnItems/index -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>  {{$title}} </title>
    <link href="http://lab.maselko.uz/favicon.ico" type="image/x-icon" rel="icon"><link href="http://lab.maselko.uz/favicon.ico" type="image/x-icon" rel="shortcut icon"><link rel="apple-touch-icon-precomposed" href="http://lab.maselko.uz/webroot/apple_touch_icon.png">

    <link rel="stylesheet" href={{asset("css/bootstrap.min.css")}}>    <link rel="stylesheet" href={{asset("css/animate.css")}}>
        </head>
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
              <a class="navbar-brand" href="http://maselko.cuz/"><i class="glyphicon glyphicon-grain"></i> Лаборатория "Маселко"</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                
   <!--  <li><a href="/"><i class="glyphicon glyphicon-book"></i> Возврат товаров</a></li> -->

   <li> <a href="http://maselko.cuz"> <i class="glyphicon glyphicon-book"> </i> <span> <h7> <font id="returnitem_color"
                @if(Route::current()->getName()=='return_items')
                color="white"
                @else color=""
                @endif
            > Возврат товаров </font>  </h7> </span>
       </a> </li>
<!-- ---------------- Добавленные кнопки на новые страницы -------------------- -->

<li> <a href="http://maselko.cuz/items"> <i class="glyphicon glyphicon-book"> </i> <span> <h7> <font size="1" id="margarine_color"
        @if(Route::current()->getName()=='items')
        color="white"
        @else color=""
        @endif
                > Продукты </font>  </h7> </span>
    </a> </li>

<li> <a href="http://maselko.cuz/agents"> <i class="glyphicon glyphicon-book"> </i> <span> <h7> <font size="1" id="mayonnaise_color"
        @if(Route::current()->getName()=='agents')
         color="white"
        @else color=""
        @endif
                > Агенты </font>  </h7> </span>
    </a> </li>

<li> <a href="http://maselko.cuz/reasons"> <i class="glyphicon glyphicon-book"> </i> <span> <h7> <font size="1" id="condensedmilk_color"
        @if(Route::current()->getName()=='reasons')
         color="white"
        @else color=""
        @endif
                > Причины возврата </font>  </h7> </span>
    </a> </li>

<li> <a href="http://maselko.cuz/citys"> <i class="glyphicon glyphicon-book"> </i> <span> <h7> <font size="1" id="creams_color"
        @if(Route::current()->getName()=='citys')
         color="white"
         @else color=""
        @endif
                > Города </font>  </h7> </span>
    </a> </li>

<li> <a href="http://maselko.cuz/decisions"> <i class="glyphicon glyphicon-book"> </i> <span> <h7> <font size="1" id="fillings_color"
        @if(Route::current()->getName()=='decisions')
          color="white"
         @else color=""
        @endif
                > Решения </font>  </h7> </span>
    </a> </li>

<!-- <li> <a href="/Fillings/index"> <i class="glyphicon glyphicon-book"> </i> <span> <h7> <font size="1" id='fillings_color' color=""> 1 </font>  </h7> </span> </a> </li> -->

<!--  <li><a href="/margarine"><i class="glyphicon glyphicon-book"></i> Маргарин</a></li>

<li><a href="/mayonnaise"><i class="glyphicon glyphicon-book"></i> Майонез</a></li>

<li><a href="/condensed_milk"><i class="glyphicon glyphicon-book"></i> Сгущенное молоко</a></li> -->
<!--
<li><a href="/creams"><i class="glyphicon glyphicon-book"></i> Декор</a></li>

<li><a href="/fillings"><i class="glyphicon glyphicon-book"></i> Начинки</a></li> -->

<!-- --------------------------------------------------------------------------------------- -->


    <li class="dropdown">
        <a href="http://lab.maselko.uz/ReturnItems/index#" class="dropdown-toggle" data-toggle="dropdown" role="button">
            <i class="glyphicon glyphicon-cog"></i>
            Настройки <span class="caret"></span>
        </a>
    <ul class="dropdown-menu">
      <li><a href="http:///maselko.cuz/items"><i class="glyphicon glyphicon-gift"></i> Продукты</a></li>
        <li><a href="http:///maselko.cuz/agents"><i class="glyphicon glyphicon-phone"></i> Агенты</a></li>
        <li><a href="http:///maselko.cuz/citys"><i class="glyphicon glyphicon-map-marker"></i> Города</a></li>
        <li><a href="http:///maselko.cuz/reasons"><i class="glyphicon glyphicon-question-sign"></i> Причины возврата</a></li>
        <li><a href="http://maselko.cuz/decisions"><i class="glyphicon glyphicon-question-sign"></i> Решения</a></li>
        <li><a href="{{asset('/marg')}}"><i class="glyphicon glyphicon-map-marker"></i> Маргарин</a></li>
         <li><a href="http://lab.maselko.uz/admin/mayonnaise"><i class="glyphicon glyphicon-map-marker"></i> Майонез</a></li>

        <li><a href="http://lab.maselko.uz/admin/condensed_milk"><i class="glyphicon glyphicon-map-marker"></i> Сгущенное молоко</a></li>
        <li><a href="http://lab.maselko.uz/admin/creamproducts"><i class="glyphicon glyphicon-map-marker"></i> Декор</a></li>
        <li><a href="http://lab.maselko.uz/admin/fillingproducts"><i class="glyphicon glyphicon-map-marker"></i> Начинки</a></li>

        <li role="separator" class="divider"></li>

        <li><a href="http://lab.maselko.uz/authentication/admin/users"><i class="glyphicon glyphicon-user"></i> Пользователи</a></li>
        <li><a href="http://lab.maselko.uz/authentication/admin/groups"><i class="glyphicon glyphicon-list-alt"></i> Группы</a></li>
        <li><a href="http://lab.maselko.uz/authentication/admin/permissions"><i class="glyphicon glyphicon-lock"></i> Разрешения</a></li>

    </ul>
  </li>


<script>  //document.getElementById("returnitem_color").color = "white"; </script>


            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="http://lab.maselko.uz/authentication/users/profile"><i class="glyphicon glyphicon-user"></i> Профиль</a></li>
<li>
    <!--<a href="http://lab.maselko.uz/authentication/users/logout"><i class="glyphicon glyphicon-off"></i> Выйти</a>-->
                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    Выход
                </a>
</li>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>



            </ul>
          </div><!--/.navbar-collapse -->
        </div>
    </nav>

    <div class="jumbotron">
      <div class="container">

      </div>

    </div>

    @yield('content')

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/moment-with-locales.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-datetimepicker.min.js')}}"></script>
    <script type="text/javascript">
        $(function () {
            $('#returned_date').datetimepicker({
                locale: 'ru',
                stepping: 10,
                format: 'YYYY-MM-DD',
                defaultDate: moment('01.01.2015').format('YYYY-MM-DD'),
                //daysOfWeekDisabled:[0,6]
            });
            $('#product_date').datetimepicker({
                locale: 'ru',
                stepping: 10,
                format: 'YYYY-MM-DD',
                defaultDate: moment('{{date("Y-m-d")}}').format('YYYY-MM-DD'),
//            daysOfWeekDisabled:[0,6]
            });
            $('#planned_date').datetimepicker({
                locale: 'ru',
                stepping: 10,
                format: 'YYYY.MM.DD',
                //defaultDate: moment('01.11.2017').format('DD.MM.YYYY'),
//            daysOfWeekDisabled:[0,6]
            });
            $('#actual_decision_date').datetimepicker({
                locale: 'ru',
                stepping: 10,
                format: 'YYYY.MM.DD',
                //defaultDate: moment('01.11.2017').format('DD.MM.YYYY'),
//            daysOfWeekDisabled:[0,6]
            });

        });
    </script>

</body></html>