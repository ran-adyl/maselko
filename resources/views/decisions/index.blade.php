    @extends('layouts.main')
    @section('content')
    <div class="container">
        <!-- Example row of columns -->
        <div class="row">
            <div class="col-md-12">

                <div class="actions columns large-2 medium-3">

    <ul class="list-inline">
        <li><a href="{{route('decisions')}}/add" class="btn btn-xs btn-primary"><i class="icon-plus"></i> Добавить решение</a></li>
        <li><a href="http://lab.maselko.uz/.csv" class="btn btn-xs btn-warning"><i class="icon-plus"></i> Экспорт CSV</a></li> 
    </ul>
</div>

<div class="returnItems index large-10 medium-9 columns">
    <table class="table table-striped">
    <thead>
    <!-- НАЗВАНИЯ СТОЛБЦОВ В ТАБЛИЦЕ -->
        <tr>
            <th><a href="http://lab.maselko.uz/?sort=item_id&amp;direction=asc">Решение</a></th>
            <th class="actions">Действия</th>
        </tr>
    <!-- - - - - - - - - - - - - - -->
    </thead>

    <tbody>

    <!-- ======== ВЫВОД ЗНАЧЕНИЙ ИЗ БАЗЫ ДАННЫХ ========== -->
    @foreach($decisions as $decision)
        <tr>

            <td><?=$decision->name?>
            </td>

            <td class="actions">
                <a href="{{route('decisions')}}/edit?id={{$decision->id}}">Изменить</a>
                <form name="post_5b2dbb7bb7ab4047066095" style="display:none;" method="post"
                      action="{{route('decisions')}}/delete?id={{$decision->id}}">
                   <input type="hidden" name="_method" value="DELETE">
                    <input type="hidden" name="id" value="{{$decision->id}}">
                    <input type="hidden" name="_token" value="csrf_token()}}">
                </form>
                <a href="{{route('decisions')}}/delete?id={{$decision->id}}"
                   onclick="if (confirm(&quot;Are you sure you want to delete # {{$decision->id}}?&quot;))
                                { document.post_5b2dbb7bb7ab4047066095.submit(); }
                                event.returnValue = false;
                                return false;
                      ">Удалить</a>
            </td>
        </tr>

    @endforeach


        <!-- ================================================ -->
    </tbody>
    </table>
    {{$decisions->links()}}
</div>            </div>
        </div>

      <hr>

      <footer>
        <p>© Maselko 2016</p>
      </footer>
    </div> <!-- /container -->
    @endsection
