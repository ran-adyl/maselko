@extends('layouts.main')
@section('content')
<div class="container">
    <!-- Example row of columns -->
    <div class="row">
        <div class="col-md-12">

            <div class="actions columns large-2 medium-3">
                <ul class="list-inline">
                    <li><a href="{{route('decisions')}}" class="btn btn-xs btn-success"><i class="icon-level-down"></i>
                            Список решений</a></li>
                </ul>
            </div>
            <div class="items form large-10 medium-9 columns">
                <form method="post" accept-charset="utf-8" role="form" action="{{route('decisions')}}/store">
                    <div style="display:none;">
                        <input type="hidden" name="_method" value="POST"></div>
                    <fieldset>

                        <div class="form-group"><label class="control-label" for="name">Наименование решения</label>
                            <input type="text" name="name" maxlength="255" id="name" class="form-control">
                        </div>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">

                    </fieldset>

                    <button type="submit" class="btn">Сохранить</button>
                </form>
            </div>
        </div>
    </div>

    <hr>

    <footer>
        <label> © Maselko 2016 </label>
    </footer>
    <!-- /container -->

</div>
@endsection