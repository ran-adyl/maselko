    @extends('layouts.main')
    @section('content')
    <div class="container">
        <!-- Example row of columns -->
        <div class="row">
            <div class="col-md-12">

                <div class="actions columns large-2 medium-3">

    <ul class="list-inline">
        <li><a href="{{route('return_items')}}/add" class="btn btn-xs btn-primary"><i class="icon-plus"></i> Добавить возврат товара</a></li>
        <li><a href="http://lab.maselko.uz/.csv" class="btn btn-xs btn-warning"><i class="icon-plus"></i> Экспорт CSV</a></li> 
    </ul>
</div>

                <form action="/" method="get"class="form-inline" role="form">
                    <table>
                        <tbody>
                        <tr>
                            <td>
                                <div class="form-group">
                                    <div class="input-group date" name="returned_date" id="returned_date">
                                        <label class="control-label">От</label>
                                        <input type="date" class="form-control" name="start" value="{{$startDate}}"/>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <div class="input-group date" name="product_date" id="product_date">
                                        <label class="control-label">До</label>
                                        <input type="date" class="form-control" name="end" value="{{$endDate}}"/>
                                    </div>
                                </div>
                            </td>
                            <td valign="bottom">
                                <button type="submit" class="btn">Найти</button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>

<div class="returnItems index large-10 medium-9 columns">
    <table class="table table-striped">
    <thead>
    <!-- НАЗВАНИЯ СТОЛБЦОВ В ТАБЛИЦЕ -->
        <tr>
            <th><a href="http://lab.maselko.uz/?sort=created&amp;direction=asc">Дата записи</a></th>
            <th><a href="http://lab.maselko.uz/?sort=return_date&amp;direction=asc">Дата возврата</a></th> 
            <th><a href="http://lab.maselko.uz/?sort=creation_date&amp;direction=asc">Дата производства</a></th>

            <th><a href="http://lab.maselko.uz/?sort=item_id&amp;direction=asc">Продукт</a></th>
            <th><a href="http://lab.maselko.uz/?sort=city_id&amp;direction=asc">Город</a></th>
            <th><a href="http://lab.maselko.uz/?sort=quantity_return_item&amp;direction=asc">Количество штук товара</a></th>
            <th><a href="http://lab.maselko.uz/?sort=agent_id&amp;direction=asc">Агент</a></th>
           
            <th><a href="http://lab.maselko.uz/?sort=id_firm&amp;direction=asc">ID фирмы</a></th>
 <!--           <th><a href="http://lab.maselko.uz/?sort=firm&amp;direction=asc">Фирма</a></th> -->
            <th><a href="http://lab.maselko.uz/?sort=reason_id&amp;direction=asc">Причина</a></th>
            <th><a href="http://lab.maselko.uz/?sort=decision_updated&amp;direction=asc">Фактическая дата принятия решения</a></th>
            <th><a href="http://lab.maselko.uz/?sort=decision_id&amp;direction=asc">Решение по возвращенному товару</a></th>
            <th><a href="http://lab.maselko.uz/?sort=email&amp;direction=asc">Пользователь, который принял решение</a></th>
            <th><a href="http://lab.maselko.uz/?sort=reason_analization&amp;direction=asc">Анализ причины возникновения возврата</a></th>
            <th><a href="http://lab.maselko.uz/?sort=correction_actions&amp;direction=asc">Кооректирующие действия</a></th>
            
            <th class="actions">Действия</th>
        </tr>
    <!-- - - - - - - - - - - - - - -->
    </thead>
    <tbody class="table-rows">
        @include('partials.return-items')
            <!-- ================================================ -->
    </tbody>
    </table>

</div>            </div>
        </div>

      <hr>

      <footer>
        <p>© Maselko 2016</p>
      </footer>
    </div> <!-- /container -->
    @endsection
