@extends('layouts.main')
@section('content')
<div class="container">
    <!-- Example row of columns -->
    <div class="row">
        <div class="col-md-12">

            <div class="actions columns large-2 medium-3">
                <ul class="list-inline">
                    <li><a href="{{route('return_items')}}" class="btn btn-xs btn-success"><i class="icon-level-down"></i>
                            Список возвращенных товаров</a></li>
                </ul>
            </div>
            <div class="returnItems form large-10 medium-9 columns">
                <form method="post" accept-charset="utf-8" role="form" action="http://maselko.cuz/store">
                    <div style="display:none;">
                        <input type="hidden" name="_method" value="POST"></div>
                    <fieldset>
                        <legend>Добавить возврат товара</legend>
                        <div class="form-group">
                            <div class="input-group date" name="returned_date" id="returned_date">
                                <label class="control-label">Дата возврата</label>
                                <input type="datetime" class="form-control" name="returned_date" id="returned_date"/>
                                <span class="input-group-addon">
				                                <span class="glyphicon-calendar glyphicon"></span>
			                                </span>
                            </div>
                        </div>

                       <div class="form-group">
                            <div class="input-group date" name="product_date" id="product_date">
                                <label class="control-label">Дата производства</label>
                                <input type="datetime" class="form-control" name="product_date" id="product_date"/>
                                <span class="input-group-addon">
				                                <span class="glyphicon-calendar glyphicon"></span>
			                                </span>
                            </div>
                        </div>


                        <div class="form-group"><label for="item-id">Продукт</label><select name="item_id" id="item-id"
                                                                                            class="form-control">
                                <option value=""></option>
                                @foreach($item as $items)
                                    <option value=<?=$items->id?>><?=$items->name?></option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group"><label for="city-id">Город</label>
                            <select name="city_id" id="city-id" class="form-control">
                                <option value=""></option>
                                @foreach($city as $cities)
                                    <option value=<?=$cities->id?>><?=$cities->name?></option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group"><label class="control-label" for="quantity">Количество штук
                                товара</label>
                            <input type="number" name="quantity" id="quantity" class="form-control"></div>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="user_id" value="1">
                        <!--                <input type="number" name="quantity_return_item" id="quantity-return-item" class="form-control"></div>-->

                        <div class="form-group"><label for="agent-id">Агент</label>
                            <select name="agent_id" id="agent-id" class="form-control">
                                <option value=""></option>
                                @foreach($agent as $agents)
                                    <option value=<?=$agents->id?>><?=$agents->name?></option>
                                @endforeach
                            </select></div>

                        <div class="form-group"><label class="control-label" for="id-firm">ID фирмы</label>
                            <input type="number" name="id_firm" id="id-firm" class="form-control">
                        </div>

                        <div class="form-group"><label class="control-label" for="firm">Фирма</label>
                            <input type="text" name="firm" maxlength="255" id="firm" class="form-control">
                        </div>

                        <div class="form-group"><label for="decision-id">Решение по возвращенному товару</label>
                            <select name="decision_id" id="decision-id" class="form-control">
                                <option value=""></option>
                                @foreach($decision as $decisions)
                                    <option value=<?=$decisions->id?>><?=$decisions->name?></option>
                                @endforeach

                            </select>
                        </div>

                        <div class="form-group">
                            <div class="input-group date" name="planned_date" id="planned_date">
                                <label class="control-label">Планируемая дата принятия решения</label>
                                <input type="datetime" class="form-control" name="planned_date" id="planned_date"/>
                                <span class="input-group-addon">
				                                <span class="glyphicon-calendar glyphicon"></span>
			                                </span>
                            </div>
                        </div>

                        <div class="form-group"><label class="control-label" for="reason-analization">Анализ причины
                                возникновения
                                возврата</label><input type="text" name="reason_analization" maxlength="255"
                                                       id="reason-analization"
                                                       class="form-control"></div>
                        <div class="form-group"><label class="control-label" for="correction-actions">Кооректирующие
                                действия</label><input type="text" name="correction_actions" maxlength="255"
                                                       id="correction-actions"
                                                       class="form-control"></div>
                        <div class="form-group"><label for="reason-id">Причина</label>
                            <select name="reason_id" id="reason-id" class="form-control">
                                <option value=""></option>
                                @foreach($reason as $reasons)
                                    <option value=<?=$reasons->id?>><?=$reasons->name?></option>
                                @endforeach
                            </select></div>

                    </fieldset>

                    <button type="submit" class="btn">Отправить</button>
                </form>
            </div>
        </div>
    </div>

    <hr>

    <footer>
        <label> © Maselko 2016 </label>
    </footer>
    <!-- /container -->

</div>
@endsection