@extends('layouts.main')

@section('content')

    <div class="container">
        <!-- Example row of columns -->
        <div class="row">
            <div class="col-md-12">

                <div class="actions columns large-2 medium-3">

                    <ul class="list-inline">
                        <li><a href="{{route('marg_items')}}/add" class="btn btn-xs btn-primary"><i class="icon-plus"></i>
                                Добавить Маргарин</a></li>
                        <li><a href="http://lab.maselko.uz/.csv" class="btn btn-xs btn-warning"><i
                                        class="icon-plus"></i> Экспорт CSV</a></li>
                    </ul>
                </div>

                <form action="/" method="get" class="form-inline" role="form">
                    <table>
                        <tbody>
                        <tr>
                            <td>
                                <div class="form-group">
                                    <div class="input-group date" name="returned_date" id="returned_date">
                                        <label class="control-label">От</label>
                                        <input type="date" class="form-control" name="start" value="{{$startDate}}"/>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <div class="input-group date" name="product_date" id="product_date">
                                        <label class="control-label">До</label>
                                        <input type="date" class="form-control" name="end" value="{{$endDate}}"/>
                                    </div>
                                </div>
                            </td>
                            <td valign="bottom">
                                <button type="submit" class="btn">Найти</button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>

                <div class="returnItems index large-10 medium-9 columns">
                    <table class="table table-striped">
                        <thead>
                        <!-- РќРђР—Р’РђРќРРЇ РЎРўРћР›Р‘Р¦РћР’ Р’ РўРђР‘Р›РР¦Р• -->
                        <tr>
                            <th><a href="http://lab.maselko.uz/margarine?sort=created&amp;direction=asc">Дата записи</a>
                            </th>
                            <th><a href="http://lab.maselko.uz/margarine?sort=Create+Date&amp;direction=asc">Дата
                                    производства</a></th>
                            <th><a href="http://lab.maselko.uz/margarine?sort=Margarine&amp;direction=asc">Название
                                    продукта</a></th>
                            <th><a href="http://lab.maselko.uz/margarine?sort=Batch&amp;direction=asc">Партия</a></th>

                            <th><a href="http://lab.maselko.uz/margarine?sort=quantity&amp;direction=asc">Количество и
                                    нумерация закладок</a></th>

                            <th><a href="http://lab.maselko.uz/margarine?sort=Kktpasteurization&amp;direction=asc">ККТ1
                                    Пастеризация</a></th>
                            <th><a href="http://lab.maselko.uz/margarine?sort=Kktfiltration&amp;direction=asc">ККТ2
                                    Фильтрация</a></th>
                            <th><a href="http://lab.maselko.uz/margarine?sort=Nett&amp;direction=asc">Масса нетто</a>
                            </th>
                            <th><a href="http://lab.maselko.uz/margarine?sort=Wrapperdata&amp;direction=asc">Внешний вид
                                    упаковки и датировка</a></th>
                            <th><a href="http://lab.maselko.uz/margarine?sort=Consistency&amp;direction=asc">Внешний
                                    вид, консистенция</a></th>
                            <th><a href="http://lab.maselko.uz/margarine?sort=Coloren&amp;direction=asc">Цвет</a></th>
                            <th><a href="http://lab.maselko.uz/margarine?sort=Taste+Smell&amp;direction=asc">Вкус,
                                    запах</a></th>

                            <th><a href="http://lab.maselko.uz/margarine?sort=Massfat&amp;direction=asc">Массовая доля
                                    жира, %</a></th>
                            <th><a href="http://lab.maselko.uz/margarine?sort=Humidity+Volatile&amp;direction=asc">М. д.
                                    влаги и летучих веществ, %</a></th>
                            <th><a href="http://lab.maselko.uz/margarine?sort=Salt&amp;direction=asc">Массовая доля
                                    соли, %</a></th>
                            <th><a href="http://lab.maselko.uz/margarine?sort=Preservative&amp;direction=asc">М. д.
                                    консерванта, %</a></th>
                            <th><a href="http://lab.maselko.uz/margarine?sort=Acidity+K&amp;direction=asc">Кислотность,
                                    K</a></th>

                            <th><a href="http://lab.maselko.uz/margarine?sort=Melting+Temperature&amp;direction=asc">Температура
                                    плавления жира, С</a></th>

                            <th><a href="http://lab.maselko.uz/margarine?sort=Bgkp&amp;direction=asc">БГКП, в 0,1 г.</a>
                            </th>
                            <th><a href="http://lab.maselko.uz/margarine?sort=Mold&amp;direction=asc">Плесень,
                                    КОЕ/г.</a></th>
                            <th><a href="http://lab.maselko.uz/margarine?sort=Yeast&amp;direction=asc">КМАФАНМ,
                                    кое/г.</a></th>
                            <th>
                                <a href="http://lab.maselko.uz/margarine?sort=Monitoring&amp;direction=asc">Мониторинг</a>
                            </th>
                            <th><a href="http://lab.maselko.uz/margarine?sort=decision_updated&amp;direction=asc">Фактическая
                                    дата принятия решения</a></th>
                            <th>
                                <a href="http://lab.maselko.uz/margarine?sort=decisionraw2_id&amp;direction=asc">Решение</a>
                            </th>
                            <th><a href="http://lab.maselko.uz/margarine?sort=planned_date&amp;direction=asc">Планируемая
                                    дата принятия решения</a></th>
                            <th><a href="http://lab.maselko.uz/margarine?sort=email&amp;direction=asc">Пользователь,
                                    который принял решение</a></th>
                            <th><a href="http://lab.maselko.uz/margarine?sort=notes&amp;direction=asc">Примечания</a>
                            </th>
                            <th class="actions">Действия</th>
                        </tr>
                        <!-- - - - - - - - - - - - - - -->
                        </thead>
                        <tbody class="table-rows">
                        @include('partials.marg-items')
                        <!-- ================================================ -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <hr>

        <footer>
            <p>© Maselko 2016</p>
        </footer>
    </div> <!-- /container -->
@endsection