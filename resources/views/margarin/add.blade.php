@extends('layouts.main')
@section('content')
<div class="container">
    <!-- Example row of columns -->
    <div class="row">
        <div class="col-md-12">

            <div class="actions columns large-2 medium-3">
                <ul class="list-inline">
                    <li><a href="{{route('marg_items')}}" class="btn btn-xs btn-success"><i class="icon-level-down"></i>
                            Список Маргарина</a></li>
                </ul>
            </div>
            <div class="returnItems form large-10 medium-9 columns">
                <form method="post" accept-charset="utf-8" role="form" action="http://maselko.cuz/marg/store">
                    <div style="display:none;">
                        <input type="hidden" name="_method" value="POST"></div>
                    <fieldset>
                        <legend>Добавить Маргарин</legend>

                       <div class="form-group">
                            <div class="input-group date" name="product_date" id="product_date">
                                <label class="control-label">Дата производства</label>
                                <input type="datetime" class="form-control" name="product_date" id="product_date">
                                <span class="input-group-addon">
				                                <span class="glyphicon-calendar glyphicon"></span>
                                </span>
                            </div>
                        </div>

                        <div class="form-group"><label class="control-label" for="product_id">Сырье</label>
                            <select name="product_id" id="product_id" class="form-control">
                                <option value=""></option>
                                @foreach($prod as $prods)
                                        <option value=<?=$prods->id?>><?=$prods->name?></option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group"><label class="control-label" for="batch">Партия</label>
                            <input type="text" name="batch" id="batch" class="form-control">
                        </div>

                        <div class="form-group"><label class="control-label" for="quantity">Количество и нумерация закладок</label>
                            <input type="number" name="quantity" id="quantity" class="form-control">
                        </div>

                        <div class="form-group"><label class="control-label" for="kkt1_pasteurization">ККТ1 Пастеризация</label>
                            <select name="kkt2_pasteurization" id="kkt1_pasteurization" class="form-control">
                                <option value=""></option>
                                @foreach($kkt1 as $kkt1s)
                                        <option value=<?=$kkt1s->id?>><?=$kkt1s->name?></option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group"><label class="control-label" for="kkt2_filtration">ККТ2 Фильтрация</label>
                            <select name="kkt2_filtration" id="kkt2_filtration" class="form-control">
                                <option value=""></option>
                                @foreach($kkt2 as $kkt2s)
                                        <option value=<?=$kkt2s->id?>><?=$kkt2s->name?></option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group"><label class="control-label" for="net_weight">Вес нетто</label>

                            <select name="net_weight" id="net_weight" class="form-control">
                                <option value=""></option>
                                @foreach($net as $nets)
                                        <option value=<?=$nets->id?>><?=$nets->name?></option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group"><label class="control-label" for="packaging">Внешний вид упаковки</label>
                            <select name="packaging" id="packaging" class="form-control">
                                <option value=""></option>
                                @foreach($pack as $packs)
                                        <option value=<?=$packs->id?>><?=$packs->name?></option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group"><label class="control-label" for="consistency">Консистенция</label>
                            <select name="consistency" id="consistency" class="form-control">
                                <option value=""></option>
                                @foreach($cons as $conss)
                                        <option value=<?=$conss->id?>><?=$conss->name?></option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group"><label class="control-label" for="color">Цвет</label>
                            <select name="color" id="color" class="form-control">
                                <option value=""></option>
                                @foreach($col as $cols)
                                        <option value=<?=$cols->id?>><?=$cols->name?></option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group"><label class="control-label" for="taste_smell">Вкус и запах</label>
                            <select name="taste_smell" id="taste_smell" class="form-control">
                                <option value=""></option>
                                @foreach($smel as $smels)
                                        <option value=<?=$smels->id?>><?=$smels->name?></option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group"><label class="control-label" for="fat">Массовая доля жира</label>
                            <input type="number" name="fat" id="fat" class="form-control">
                        </div>

                        <div class="form-group"><label class="control-label" for="humidity">Влажность и кислотность</label>
                            <input type="number" name="humidity" id="humidity" class="form-control">
                        </div>

                        <div class="form-group"><label class="control-label" for="salt">Доля соли</label>
                            <input type="number" name="salt" id="salt" class="form-control">
                        </div>

                        <div class="form-group"><label class="control-label" for="preservative">Массовая доля консервантов</label>
                            <input type="number" name="preservative" id="preservative" class="form-control">
                        </div>

                        <div class="form-group"><label class="control-label" for="avidity_k">Кислотность К</label>
                            <input type="number" name="avidity_k" id="avidity_k" class="form-control">
                        </div>

                        <div class="form-group"><label class="control-label" for="melting_temperature">Температура плавления</label>
                            <input type="number" name="melting_temperature" id="melting_temperature" class="form-control">
                        </div>

                        <div class="form-group"><label class="control-label" for="coliform_bacteria">БГКП (доля колиформных бактерий)</label>
                            <input type="number" name="coliform_bacteria" id="coliform_bacteria" class="form-control">
                        </div>

                        <div class="form-group"><label class="control-label" for="mold">Плесеньробные</label>
                            <input type="number" name="mold" id="mold" class="form-control">
                        </div>

                        <div class="form-group"><label class="control-label" for="anaerobic_microorganisms">КМАФНМ (анаэ бактерии)А</label>
                            <input type="number" name="anaerobic_microorganisms" id="anaerobic_microorganisms" class="form-control">
                        </div>

                        <div class="form-group"><label class="control-label" for="monitoring">Мониторинг</label>
                            <input type="number" name="monitoring" id="monitoring" class="form-control">
                        </div>

                        <div class="form-group">
                            <div class="input-group date" name="actual_decision_date" id="actual_decision_date">
                                <label class="control-label">Фактическая дата принятия решения</label>
                                <input type="datetime" class="form-control" name="actual_decision_date" id="actual_decision_date"/>
                                <span class="input-group-addon">
				                                <span class="glyphicon-calendar glyphicon"></span>
                                </span>
                            </div>
                        </div>

                        <div class="form-group"><label class="control-label" for="dec2raw">Решение</label>
                            <select name="dec2raw" id="dec2raw" class="form-control">
                                <option value=""></option>
                                @foreach($dec2raw as $dec2raws)
                                        <option value=<?=$dec2raws->id?>><?=$dec2raws->name?></option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <div class="input-group date" name="planned_date" id="planned_date">
                                <label class="control-label">Планируемая дата принятия решения</label>
                                <input type="datetime" class="form-control" name="planned_date" id="planned_date"/>
                                <span class="input-group-addon">
				                                <span class="glyphicon-calendar glyphicon"></span>
                                </span>
                            </div>
                        </div>

                        <div class="form-group"><label class="control-label" for="comments">Комментарий</label>
                            <input type="text" name="comments" id="comments" class="form-control">
                        </div>

                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="user_id" value="1">
                        <!--                <input type="number" name="quantity_return_item" id="quantity-return-item" class="form-control"></div>-->


                    </fieldset>

                    <button type="submit" class="btn">Отправить</button>
                </form>
            </div>
        </div>
    </div>

    <hr>

    <footer>
        <label> © Maselko 2016 </label>
    </footer>
    <!-- /container -->

</div>
@endsection