@extends('layouts.main')
@section('content')
    <div class="container">
        <!-- Example row of columns -->
        <div class="row">
            <div class="col-md-12">

                <div class="actions columns large-2 medium-3">
                    <ul class="list-inline">
                        <li><a href="{{route('marg_items')}}" class="btn btn-xs btn-success"><i class="icon-level-down"></i>
                                Список Маргарина</a></li>
                    </ul>
                </div>
                <div class="returnItems form large-10 medium-9 columns">
                    <form method="post" accept-charset="utf-8" role="form" action="http://maselko.cuz/marg/update">
                        <div style="display:none;">
                            <input type="hidden" name="_method" value="PUT"></div>
                        <fieldset>
                            <legend>Изменить Маргарин</legend>

                            <div class="form-group">
                                <div class="input-group date" name="product_date" id="product_date">
                                    <label class="control-label">Дата производства</label>
                                    <input type="datetime" class="form-control" name="product_date" id="product_date" value={{$margItems->product_date}}>
                                    <span class="input-group-addon">
				                                <span class="glyphicon-calendar glyphicon"></span>
                                </span>
                                </div>
                            </div>

                            <input type="hidden" name="id" value="{{$margItems->id}}">

                            <div class="form-group"><label class="control-label" for="product_id">Сырье</label>
                                <select name="product_id" id="product_id" class="form-control">
                                    <option value=""></option>
                                    @foreach($prod as $prods)
                                        @if($prods->id==$margItem->product_id)
                                            <option value=<?=$prods->id?> selected="selected"><?=$prods->name?></option>
                                        @else
                                            <option value=<?=$prods->id?>><?=$prods->name?></option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group"><label class="control-label" for="batch">Партия</label>
                                <input type="text" name="batch" id="batch" class="form-control" value={{$margItems->batch}}>
                            </div>

                            <div class="form-group"><label class="control-label" for="quantity">Количество и нумерация закладок</label>
                                <input type="number" name="quantity" id="quantity" class="form-control" value={{$margItems->quantity}}>
                            </div>

                            <div class="form-group"><label class="control-label" for="kkt1_pasteurization">ККТ1 Пастеризация</label>
                                <select name="kkt2_pasteurization" id="kkt1_pasteurization" class="form-control">
                                    <option value=""></option>
                                    @foreach($kkt1 as $kkt1s)
                                        @if($kkt1s->id==$margItem->kkt1_pasteurization)
                                            <option value=<?=$kkt1s->id?> selected="selected"><?=$kkt1s->name?></option>
                                        @else
                                            <option value=<?=$kkt1s->id?>><?=$kkt1s->name?></option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group"><label class="control-label" for="kkt2_filtration">ККТ2 Фильтрация</label>
                                <select name="kkt2_filtration" id="kkt2_filtration" class="form-control">
                                    <option value=""></option>
                                    @foreach($kkt2 as $kkt2s)
                                        @if($kkt2s->id==$margItem->kkt2_filtration)
                                            <option value=<?=$kkt2s->id?> selected="selected"><?=$kkt2s->name?></option>
                                        @else
                                            <option value=<?=$kkt2s->id?>><?=$kkt2s->name?></option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group"><label class="control-label" for="net_weight">Вес нетто</label>

                                <select name="net_weight" id="net_weight" class="form-control">
                                    <option value=""></option>
                                    @foreach($net as $nets)
                                        @if($nets->id==$margItem->net_weight)
                                            <option value=<?=$nets->id?> selected="selected"><?=$nets->name?></option>
                                        @else
                                            <option value=<?=$nets->id?>><?=$nets->name?></option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group"><label class="control-label" for="packaging">Внешний вид упаковки</label>
                                <select name="packaging" id="packaging" class="form-control">
                                    <option value=""></option>
                                    @foreach($pack as $packs)
                                        @if($packs->id==$margItem->packaging)
                                            <option value=<?=$packs->id?> selected="selected"><?=$packs->name?></option>
                                        @else
                                            <option value=<?=$packs->id?>><?=$packs->name?></option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group"><label class="control-label" for="consistency">Консистенция</label>
                                <select name="consistency" id="consistency" class="form-control">
                                    <option value=""></option>
                                    @foreach($cons as $conss)
                                        @if($conss->id==$margItem->consistency)
                                            <option value=<?=$conss->id?> selected="selected"><?=$conss->name?></option>
                                        @else
                                            <option value=<?=$conss->id?>><?=$conss->name?></option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group"><label class="control-label" for="color">Цвет</label>
                                <select name="color" id="color" class="form-control">
                                    <option value=""></option>
                                    @foreach($col as $cols)
                                        @if($cols->id==$margItem->color)
                                            <option value=<?=$cols->id?> selected="selected"><?=$cols->name?></option>
                                        @else
                                            <option value=<?=$cols->id?>><?=$cols->name?></option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group"><label class="control-label" for="taste_smell">Вкус и запах</label>
                                <select name="taste_smell" id="taste_smell" class="form-control">
                                    <option value=""></option>
                                    @foreach($smel as $smels)
                                        @if($smels->id==$margItem->taste_smell)
                                            <option value=<?=$smels->id?> selected="selected"><?=$smels->name?></option>
                                        @else
                                            <option value=<?=$smels->id?>><?=$smels->name?></option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group"><label class="control-label" for="fat">Массовая доля жира</label>
                                <input type="text" name="fat" id="fat" class="form-control" value={{$margItems->fat}}>
                            </div>

                            <div class="form-group"><label class="control-label" for="humidity">Влажность и кислотность</label>
                                <input type="text" name="humidity" id="humidity" class="form-control" value={{$margItems->humidity}}>
                            </div>

                            <div class="form-group"><label class="control-label" for="salt">Доля соли</label>
                                <input type="text" name="salt" id="salt" class="form-control" value={{$margItems->salt}}>
                            </div>

                            <div class="form-group"><label class="control-label" for="preservative">Массовая доля консервантов</label>
                                <input type="text" name="preservative" id="preservative" class="form-control" value={{$margItems->preservative}}>
                            </div>

                            <div class="form-group"><label class="control-label" for="avidity_k">Кислотность</label>
                                <input type="text" name="avidity_k" id="avidity_k" class="form-control" value={{$margItems->avidity_k}}>
                            </div>

                            <div class="form-group"><label class="control-label" for="melting_temperature">Температура плавления</label>
                                <input type="text" name="melting_temperature" id="melting_temperature" class="form-control" value={{$margItems->melting_temperature}}>
                            </div>

                            <div class="form-group"><label class="control-label" for="coliform_bacteria">БГКП (доля колиформных бактерий)</label>
                                <input type="text" name="coliform_bacteria" id="coliform_bacteria" class="form-control" value={{$margItems->coliform_bacteria}}>
                            </div>

                            <div class="form-group"><label class="control-label" for="mold">Плесеньробные</label>
                                <input type="text" name="mold" id="mold" class="form-control" value={{$margItems->mold}}>
                            </div>

                            <div class="form-group"><label class="control-label" for="anaerobic_microorganisms">КМАФНМ (анаэ бактерии)А</label>
                                <input type="text" name="anaerobic_microorganisms" id="anaerobic_microorganisms" class="form-control" value={{$margItems->anaerobic_microorganisms}}>
                            </div>

                            <div class="form-group"><label class="control-label" for="monitoring">Мониторинг</label>
                                <input type="text" name="monitoring" id="monitoring" class="form-control" value={{$margItems->monitoring}}>
                            </div>

                            <div class="form-group">
                                <div class="input-group date" name="actual_decision_date" id="actual_decision_date">
                                    <label class="control-label">Актуальная Дата принятия решения</label>
                                    <input type="datetime" class="form-control" name="actual_decision_date" id="actual_decision_date" value={{$margItems->actual_decision_date}}>
                                    <span class="input-group-addon">
				                                <span class="glyphicon-calendar glyphicon"></span>
                                </span>
                                </div>
                            </div>

                            <div class="form-group"><label class="control-label" for="dec2raw">Решение</label>
                                <select name="dec2raw" id="dec2raw" class="form-control">
                                    <option value=""></option>
                                    @foreach($dec2raw as $dec2raws)
                                        @if($dec2raws->id==$margItem->taste_dec2rawl)
                                            <option value=<?=$dec2raws->id?> selected="selected"><?=$dec2raws->name?></option>
                                        @else
                                            <option value=<?=$dec2raws->id?>><?=$dec2raws->name?></option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <div class="input-group date" name="planned_date" id="planned_date">
                                    <label class="control-label">Дата решения</label>
                                    <input type="datetime" class="form-control" name="planned_date" id="planned_date" value={{$margItems->planned_date}}>
                                    <span class="input-group-addon">
				                                <span class="glyphicon-calendar glyphicon"></span>
                                </span>
                                </div>
                            </div>

                            <div class="form-group"><label class="control-label" for="comments">Комментарий</label>
                                <input type="text" name="comments" id="comments" class="form-control" value={{$margItems->comments}}>
                            </div>

                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="user_id" value="1">
                            <!--                <input type="number" name="quantity_return_item" id="quantity-return-item" class="form-control"></div>-->


                        </fieldset>

                        <button type="submit" class="btn">Отправить</button>
                    </form>
                </div>
            </div>
        </div>

        <hr>

        <footer>
            <label> © Maselko 2016 </label>
        </footer>
        <!-- /container -->

    </div>
@endsection