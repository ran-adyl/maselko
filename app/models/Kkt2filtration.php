<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kkt2Filtration extends Model
{
    //
    protected $table='Kkt2filtrations';
    //
    protected $fillable=['name'];

}
