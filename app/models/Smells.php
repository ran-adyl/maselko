<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Smells extends Model
{
    //
    protected $table='Smells';
    //
    protected $fillable=['name'];

}
