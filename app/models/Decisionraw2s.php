<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Decisionraw2S extends Model
{
    //
    protected $table='Decisionraw2s';
    //
    protected $fillable=['name'];

}
