<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RawMargarine extends Model
{
    //
    protected $table='raw_margarines';

    protected $guarded=[];

    public function kkt1filtrations()
    {
        return $this->belongsTo('App\Models\Kkt2Filtration','Kkt2Filtration','id');
    }
    public function Kkt2pausterizations()
    {
        return $this->belongsTo('App\Models\Kkt1Pasteurization','Kkt1Pasteurization','id');

    }
    public function smells()
    {
        return $this->belongsTo('App\Models\Smells','taste_smells','id');
    }

    public function products()
    {
        return $this->belongsTo('App\Models\Product','product_id','id');
    }

    public function colors()
    {
        return $this->belongsTo('App\Models\Color','color','id');
    }


    public function netts()
    {
        return $this->belongsTo('App\Models\Nett','net_weight','id');
    }
    public function packagings()
    {
        return $this->belongsTo('App\Models\Packaging','packaging','id');
    }

    public function consistencys()
    {
        return $this->belongsTo('App\Models\Consistency', 'consistency_id', 'id');
    }
    public function decisionraw2S()
    {
        return $this->belongsTo('App\Models\decisionraw2S', 'decisionraw2s_id', 'id');
    }
}
