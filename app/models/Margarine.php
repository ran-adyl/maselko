<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Margarine extends Model
{
    //
    protected $table='Margarine';
    //
    protected $fillable=['name'];

}
