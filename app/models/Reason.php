<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Reason extends Model
{
    //
    protected $table='Reasons';
    protected $fillable=['name'];
}
