<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kkt1Pasteurization extends Model
{
    //
    protected $table='Kkt1pasteurizations';
    //
    protected $fillable=['name'];

}
