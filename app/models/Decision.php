<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Decision extends Model
{
    protected $table='Decisions';
    //
    protected $fillable=['name'];
}
