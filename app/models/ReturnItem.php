<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReturnItem extends Model
{
    protected $table='return_Items';

    public function reasons()
    {
        return $this->belongsTo('App\Models\Reason','reason_id','id');
    }
    public function agents()
    {
        return $this->belongsTo('App\Models\Agent','agent_id','id');
    }
    public function cities()
    {
        return $this->belongsTo('App\Models\Citie','city_id','id');
    }
    public function items()
    {
        return $this->belongsTo('App\Models\Item','item_id','id');
    }
    public function decisions()
    {
        return $this->belongsTo('App\Models\Decision','decision_id','id');
    }

    protected $fillable=['returned_date', 'product_date', 'city_id', 'agent_id', 'reason_id',
        'decision_id', 'reason_analization', 'correction_actions', 'decision_id', 'reason_analization',
        'correction_actions', 'desicion_edited', 'quantity', 'item_id', 'firm', 'user_id'];
    //$created_date = $request->input('creation_date');

}
