<?php

namespace App\Http\Controllers;

use App\Models\Consistency;
use Illuminate\Http\Request;
use App\Models\RawMargarine;

use App\Models\Kkt2Filtration;
use App\Models\Kkt1Pasteurization;
use App\Models\Decisionraw2S;
use App\Models\Nett;
use App\Models\Smells;
use App\Models\Color;
use App\Models\Product;
use App\Models\Packaging;
use App\Models\Consistency2;


class Raw_margarinController extends Controller
{
    //
    public function index(){
        $startDate=request('start');
        $endDate=request('end');
        $query = RawMargarine::query();

        if (isset($startDate)) {
            $query->where('created_at', '>=', $startDate);
        }
        else $startDate='2015-01-01';

        if (isset($endDate)) {
            $query->where('created_at', '<=', $endDate);
        }
        else $endDate=date('Y-M-D');

        $margItems = $query->paginate(20);

/*        if ($request->ajax()) {
            return view('partials.marg-items', ['margItems' => $margItem])->render();
        }*/
        return view('margarin.index', compact('margItems','startDate','endDate'))->with('title','Маргарин');

    }
    public function add()
    {
        $kkt2 = Kkt2Filtration::query()->get();
        $kkt1 = Kkt1Pasteurization::query()->get();
        $dec2raw = Decisionraw2S::query()->get();
        $pack = Packaging::query()->get();

        $prod = Product::query()->get();
        $smel = Smells::query()->get();
        $net = Nett::query()->get();
        $col = Color::query()->get();
        $cons = Consistency2::query()->get();

        return view('margarin.add',compact('kkt1','kkt2','dec2raw','col','pack','prod','smel','net','cons','dec2raw'))
        ->with('title','Маргарин. Добавление');
    }

    public function edit()
    {
        $kkt2 = Kkt2Filtration::query()->get();
        $kkt1 = Kkt1Pasteurization::query()->get();
        $dec2raw = Decisionraw2S::query()->get();
        $pack = Packaging::query()->get();

        $prod = Product::query()->get();
        $smel = Smells::query()->get();
        $net = Nett::query()->get();
        $col = Color::query()->get();
        $cons = Consistency2::query()->get();

        $id=request('id');
        $margItems=RawMargarine::query()->where(['id'=>$id])->first();
        //return view('margarin.edit', ['item' => $items, 'agent' => $agents, 'city' => $cities, 'decision' => $decisions,
        //    'reason' => $reasons,'requestItem'=>$requstItem])->with('title','Маргарин. Редактирование');
        return view('margarin.edit', compact('margItems','kkt1','kkt2','dec2raw','col','pack','prod','smel','net'
        ,'cons','dec2raw'))
        ->with('title','Маргарин. Редактирование');
    }

    public function delete()
    {
        $id=request('id');
        $requestItem=RawMargarine::query()->where(['id'=>$id])->first();
        $requestItem->delete();
        return redirect("/marg");
    }
    public function update()
    {
        $id=request('id');
        $marg=RawMargarine::query()->where(['id'=>$id])->first();

        //dd(request());
        $marg->fill(request(['product_id', 'batch','quantity', 'kkt1_pasteurization', 'kkt2_filtration', 'net_weight', 'packaging',
            'consistency', 'color', 'taste_smell', 'fat', 'humidity', 'salt', 'preservative',
            'avidity_k', 'melting_temperature', 'coliform_bacteria', 'mold', 'anaerobic_microorganisms',
            'monitoring', 'actual_decision_date', 'planned_date', 'user_id', 'comments'
        ]));

        $marg->save();

        return redirect('/marg');

    }

    public function store()
    {

        RawMargarine::create(request(['product_id', 'batch','quantity', 'kkt1_pasteurization', 'kkt2_filtration', 'net_weight', 'packaging',
            'consistency', 'color', 'taste_smell', 'fat', 'humidity', 'salt', 'preservative',
            'avidity_k', 'melting_temperature', 'coliform_bacteria', 'mold', 'anaerobic_microorganisms',
            'monitoring', 'actual_decision_date', 'planned_date', 'user_id', 'comments'
        ]));

        return redirect('/marg');
    }

}
