<?php

namespace App\Http\Controllers;

use App\Models\Item;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    //
    public function index()
    {
        $query=Item::query();
        $item=$query->paginate(20);
        return view('items.index',['items' => $item])->with('title','Справочник продуктов');
    }
    public function add()
    {
        return view('items.add')->with('title','Справочник продуктов. Добавление');
    }

    public function edit(Request $request)
    {
        $id=$request->input('id');
        $requstItem=Item::query()->where(['id'=>$id])->first();
        return view('items.edit', ['requestItem'=>$requstItem])->with('title','Справочник продуктов. Редактирование');
    }

    public function delete(Request $request)
    {
        $id=$request->input('id');
        $requestItem=Item::query()->where(['id'=>$id])->first();
        $requestItem->delete();
        return redirect("/items");
    }
    public function update(Request $request)
    {

        $id = $request->input('id');
        $item= Item::query()->where(['id'=>$id])->first();

        $item->fill($request->input());

        $item->save();

        return redirect('/items');
    }

    public function store(Request $request)
    {


        $item = new Item();

        $item->fill($request->input());

        $item->save();
        return redirect('/items');
    }

}
