<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\ReturnItem;
use App\models\Agent;
use App\models\Citie;
use App\models\Decision;
use App\models\Item;
use App\models\Reason;
use phpDocumentor\Reflection\Types\Null_;


class ReturnItemController extends Controller
{
    public function index(Request $request)

    {
        $startDate=$request->input('start');
        $endDate=$request->input('end');
        $query = ReturnItem::query();

        if (isset($startDate)) {
            $query->where('created_at', '>=', $startDate);
        }
        else $startDate='2015-01-01';

        if (isset($endDate)) {
            $query->where('created_at', '<=', $endDate);
        }
        else $endDate=date('Y-M-D');

        $returnItem = $query->paginate(20);

        if ($request->ajax()) {
            return view('partials.return-items', ['returnItems' => $returnItem])->render();
        }
        return view('return_items.index', ['returnItems' => $returnItem,'startDate'=>$startDate,'endDate'=>$endDate])->with('title','Возврат продуктов');
    }

    public function add()
    {
        $agents = Agent::query()->get();
        $cities = Citie::query()->get();
        $decisions = Decision::query()->get();
        $items = Item::query()->get();
        $reasons = reason::query()->get();
        return view('return_items.add', ['item' => $items, 'agent' => $agents, 'city' => $cities, 'decision' => $decisions,
            'reason' => $reasons])->with('title','Возврат продуктов. Добавление');
    }

    public function edit(Request $request)
    {
        $agents = Agent::query()->get();
        $cities = Citie::query()->get();
        $decisions = Decision::query()->get();
        $items = Item::query()->get();
        $reasons = reason::query()->get();
        $id=$request->input('id');
        $requstItem=ReturnItem::query()->where(['id'=>$id])->first();
        return view('return_items.edit', ['item' => $items, 'agent' => $agents, 'city' => $cities, 'decision' => $decisions,
            'reason' => $reasons,'requestItem'=>$requstItem])->with('title','Возврат продуктов. Редактирование');
    }

    public function delete(Request $request)
    {
        $id=$request->input('id');
        $requestItem=ReturnItem::query()->where(['id'=>$id])->first();
        $requestItem->delete();
        return redirect("/");
    }
    public function update(Request $request)
    {

        $id = $request->input('id');
        $returnItem= ReturnItem::query()->where(['id'=>$id])->first();

        $returnItem->fill($request->input());


        $returnItem->save();


        return redirect('/');
    }

    public function store(Request $request)
    {


        $returnItem = new ReturnItem();

        $returnItem->fill($request->input());

        $returnItem->save();
        return redirect('/');
    }


    //
}
