<?php

namespace App\Http\Controllers;

use App\Models\Decision;
use Illuminate\Http\Request;

class DecisionController extends Controller
{
    //
    public function index()
    {
        $query=Decision::query();
        $decision=$query->paginate(20);
        return view('decisions.index',['decisions' => $decision])->with('title','Справочник решений');
    }
    public function add()
    {
        return view('decisions.add')->with('title','Справочник решений. Добавление');
    }

    public function edit(Request $request)
    {
        $id=$request->input('id');
        $requstdecision=Decision::query()->where(['id'=>$id])->first();
        return view('decisions.edit', ['requestdecision'=>$requstdecision])->with('title','Справочник решений. Редактирование');
    }

    public function delete(Request $request)
    {
        $id=$request->input('id');
        $requestdecision=Decision::query()->where(['id'=>$id])->first();
        $requestdecision->delete();
        return redirect('/decisions');
    }
    public function update(Request $request)
    {

        $id = $request->input('id');
        $decision= Decision::query()->where(['id'=>$id])->first();

        $decision->fill($request->input());

        $decision->save();

        return redirect('/decisions');
    }

    public function store(Request $request)
    {


        $decision = new Decision();

        $decision->fill($request->input());

        $decision->save();
        return redirect('/decisions');
    }

}
