<?php

namespace App\Http\Controllers;

use App\Models\Agent;
use Illuminate\Http\Request;

class AgentsController extends Controller
{
    //
    public function index()
    {
        $query=Agent::query();
        $agent=$query->paginate(20);
        return view('agents.index',['agents' => $agent])->with('title','Справочник агентов');
    }
    public function add()
    {
        return view('agents.add')->with('title','Справочник агентов. Добавление');
    }

    public function edit(Request $request)
    {
        $id=$request->input('id');
        $requstagent=Agent::query()->where(['id'=>$id])->first();
        return view('agents.edit', ['requestagent'=>$requstagent])->with('title','Справочник агентов. Редактирование');
    }

    public function delete(Request $request)
    {
        $id=$request->input('id');
        $requestagent=Agent::query()->where(['id'=>$id])->first();
        $requestagent->delete();
        return redirect("/agents");
    }
    public function update(Request $request)
    {

        $id = $request->input('id');
        $agent= agent::query()->where(['id'=>$id])->first();

        $agent->fill($request->input());

        $agent->save();

        return redirect('/agents');
    }

    public function store(Request $request)
    {


        $agent = new Agent();

        $agent->fill($request->input());

        $agent->save();
        return redirect('/agents');
    }

}
