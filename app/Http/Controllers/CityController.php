<?php

namespace App\Http\Controllers;

use App\Models\Citie;
use Illuminate\Http\Request;


class CityController extends Controller
{
    //
    public function index()
    {
        $query=Citie::query();
        $city=$query->paginate(20);
        return view('citys.index',['citys' => $city])->with('title','Справочник городов');
    }
    public function add()
    {
        return view('citys.add')->with('title','Справочник городов. Добавление');
    }

    public function edit(Request $request)
    {
        $id=$request->input('id');
        $requstcity=Citie::query()->where(['id'=>$id])->first();
        return view('citys.edit', ['requestcity'=>$requstcity])->with('title','Справочник городов. Редактирование');
    }

    public function delete(Request $request)
    {
        $id=$request->input('id');
        $requestcity=Citie::query()->where(['id'=>$id])->first();
        $requestcity->delete();
        return redirect('/citys');
    }
    public function update(Request $request)
    {

        $id = $request->input('id');
        $city= Citie::query()->where(['id'=>$id])->first();

        $city->fill($request->input());

        $city->save();

        return redirect('/citys');
    }

    public function store(Request $request)
    {


        $city = new Citie();

        $city->fill($request->input());

        $city->save();
        return redirect('/citys');
    }

}
