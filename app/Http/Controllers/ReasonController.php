<?php

namespace App\Http\Controllers;

use App\Models\Reason;
use Illuminate\Http\Request;

class ReasonController extends Controller
{
    //
    public function index()
    {
        $query=Reason::query();
        $reason=$query->paginate(20);
        return view('reasons.index',['reasons' => $reason])->with('title','Справочник причин возврата');
    }
    public function add()
    {
        return view('reasons.add')->with('title','Справочник причин возврата. Добавление');
    }

    public function edit(Request $request)
    {
        $id=$request->input('id');
        $requstreason=Reason::query()->where(['id'=>$id])->first();
        return view('reasons.edit', ['requestreason'=>$requstreason])->with('title','Справочник причин возврата. Редактирование');
    }

    public function delete(Request $request)
    {
        $id=$request->input('id');
        $requestreason=Reason::query()->where(['id'=>$id])->first();
        $requestreason->delete();
        return redirect("/reasons");
    }
    public function update(Request $request)
    {

        $id = $request->input('id');
        $reason= reason::query()->where(['id'=>$id])->first();

        $reason->fill($request->input());

        $reason->save();

        return redirect('/reasons');
    }

    public function store(Request $request)
    {


        $reason = new Reason();

        $reason->fill($request->input());

        $reason->save();
        return redirect('/reasons');
    }

}
